-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2021 at 11:54 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `food_app_hkr`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'vegan'),
(2, 'milk'),
(3, 'meat'),
(4, 'vegetables'),
(5, 'sugar'),
(6, 'fruits'),
(7, 'grill'),
(9, 'pet food'),
(10, 'cat food'),
(11, 'dog food'),
(12, 'dry pet food'),
(14, 'drinks'),
(16, 'snacks'),
(17, 'other'),
(18, 'nuts');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `sub_name` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `vendor` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `country_of_production` varchar(56) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id`, `name`, `sub_name`, `vendor`, `country_of_production`, `description`) VALUES
(2, 'Port salut 600', '', 'Skånemejerier', 'Sweden', ''),
(4, 'Marabou nuts', '', 'Marabou', 'Sweden', ''),
(5, 'Marabou milk', '', 'Marabou', 'Sweden', ''),
(7, 'Marabou havsalt', '', 'Marabou', 'Sweden', ''),
(13, 'Ben & Jerry', '', 'Ben & Jerry', 'USA', ''),
(14, 'Orbit mint', '', 'Orbit', 'Estonia', ''),
(15, 'Orbit fruit', '', 'Orbit', 'Estonia', ''),
(17, 'Orbit melon', '', 'Orbit', 'Estonia', ''),
(42, 'Whiskas junior', '', 'citty', 'Germany', ''),
(47, 'Whiskas 6x100', '', 'Kaven Orbico', 'Germany', ''),
(48, 'Chappy 500 chicken', '', 'Kaven Orbico', 'Italy', ''),
(49, 'Chappy 200 chicken', '', 'Kaven Orbico', 'Italy', ''),
(50, 'Pedigree junior', '', 'Kaven Orbico', 'Italy', ''),
(51, 'Royal canin 500gr', '', 'Royal canin', 'Switzerland', ''),
(52, 'White Beans', '', 'beany', 'Bulgaria', ''),
(53, 'Cola', '', 'Coca Cola', 'Belgium', ''),
(55, 'Chips', '', 'Owe', 'Sweden', ''),
(57, 'Tomatoes', '', 'Eko Swe', 'Sweden', ''),
(59, 'Gurka', '', 'Eko Sv', 'Sweden', '');

-- --------------------------------------------------------

--
-- Table structure for table `item_category`
--

CREATE TABLE `item_category` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item_category`
--

INSERT INTO `item_category` (`id`, `item_id`, `category_id`) VALUES
(2, 2, 4),
(3, 2, 6),
(4, 2, 5),
(7, 17, 1),
(22, 53, 14),
(24, 55, 16),
(25, 48, 11),
(26, 48, 12),
(27, 7, 5),
(28, 15, 1),
(29, 50, 11),
(30, 5, 5),
(31, 5, 16),
(32, 2, 2),
(33, 5, 5),
(41, 4, 5),
(42, 4, 2),
(43, 57, 4),
(44, 42, 10),
(45, 47, 10),
(46, 49, 11),
(47, 51, 10),
(48, 51, 12),
(49, 52, 17),
(50, 59, 4);

-- --------------------------------------------------------

--
-- Table structure for table `on_sale`
--

CREATE TABLE `on_sale` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `original_price` decimal(5,2) NOT NULL,
  `current_price` decimal(5,2) NOT NULL,
  `offer_created` datetime NOT NULL,
  `available_quantity` decimal(10,0) NOT NULL,
  `quantity_type_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `on_sale`
--

INSERT INTO `on_sale` (`id`, `item_id`, `store_id`, `original_price`, `current_price`, `offer_created`, `available_quantity`, `quantity_type_id`, `status_id`) VALUES
(3, 7, 1, '18.50', '8.00', '2021-05-31 01:15:47', '45', 4, 1),
(4, 2, 4, '96.90', '30.90', '2021-05-31 01:54:21', '122', 1, 1),
(5, 2, 6, '95.90', '30.90', '2021-05-31 01:54:21', '15', 1, 1),
(8, 15, 1, '12.00', '3.00', '2021-06-03 13:47:56', '130', 1, 1),
(9, 7, 1, '12.00', '3.00', '2021-06-03 13:49:08', '130', 1, 1),
(10, 50, 3, '12.00', '3.00', '2021-06-03 13:56:54', '130', 2, 1),
(14, 55, 4, '18.50', '3.00', '2021-06-03 14:19:56', '130', 2, 1),
(15, 48, 6, '28.90', '12.90', '2021-06-03 14:28:36', '78', 7, 1),
(17, 52, 1, '12.00', '3.90', '2021-06-06 13:22:10', '950', 7, 1),
(18, 53, 5, '18.90', '6.00', '2021-06-07 11:24:43', '210', 7, 1),
(19, 42, 5, '20.00', '4.00', '2021-06-07 11:26:03', '40', 3, 1),
(20, 57, 5, '32.90', '6.90', '2021-06-07 11:43:23', '41', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `quantity_type`
--

CREATE TABLE `quantity_type` (
  `id` int(11) NOT NULL,
  `name` varchar(5) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `quantity_type`
--

INSERT INTO `quantity_type` (`id`, `name`) VALUES
(1, 'kg'),
(2, 'gr'),
(3, 'pack'),
(4, 'hg'),
(5, 'l'),
(6, 'ml'),
(7, 'st');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `name` varchar(12) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `name`) VALUES
(1, 'active'),
(2, 'disabled'),
(3, 'deleted'),
(4, 'under invest'),
(5, 'reported'),
(6, 'not active');

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(86) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`id`, `name`, `city`, `address`, `phone`) VALUES
(1, 'ICA KVANTUM', 'Kristianstad', 'Stridsvagnsvägen 1, 291 39', '044-20 84 80'),
(3, 'Lidl', 'Kristianstad', 'Fäladsgatan 1, 291 59', '020-190 80 70'),
(4, 'Lidl', 'Kristianstad', 'Fundationsvägen 17, 291 50', '020-190 80 70'),
(5, 'Lidl Knivgatan', 'Malmö', 'Knivgatan 1B, 212 28', '020-190 80 70'),
(6, 'City Gross', 'Kristianstad', 'Fundationsvägen 9, 291 28 ', '044-780 85 00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `first_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(85) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `first_name`, `last_name`, `email`, `phone`, `city`) VALUES
(1, 'admin', '1', 'Admin', '', 'app@foody.se', '0790111222', 'Kristianstad'),
(2, 'test', '1', 'Rositsa', 'Nikolova', 'rosi@gmail.com', '0797790790', 'Kristianstad'),
(3, 'rositsa', 'Password123', 'Rositsa', 'Nikolova', 'rositsa@gmail.com', '079008845', 'Kristianstad'),
(4, 'rositsa1', 'Password123', 'Ros', 'Nik', 'ro@ni', '835235', 'Vienna'),
(5, 'sunny', 'Pa233312', 'Ros', 'Nik', 'ro@nik.se', '32852332', 'Kristianstad'),
(6, 'NewUser', 'Parola123', 'Rositsa', 'Nikolova', 'ros@itsa.nik', '023423566', 'Malmö'),
(7, 'Tony25', 'FoodyPass1!', 'New', 'User', 'new@user.se', '320498', 'Tollarp'),
(11, '12345678911234567891', 'Pa123456789134567891', 'Test', 'Testov', 'test@tov.te', '320589', 'Malmo'),
(12, 'username20', 'Parola123!', 'Marika', 'Nilsson', 'user@domain.org', '078546565', 'Ystad');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_category`
--
ALTER TABLE `item_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `on_sale`
--
ALTER TABLE `on_sale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quantity_type`
--
ALTER TABLE `quantity_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `item_category`
--
ALTER TABLE `item_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `on_sale`
--
ALTER TABLE `on_sale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `quantity_type`
--
ALTER TABLE `quantity_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE `store`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `item_category`
--
ALTER TABLE `item_category`
  ADD CONSTRAINT `fk_category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_item_id` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
