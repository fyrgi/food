package com.company;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserTest {
    private User user = new User();
    MyJDBC MyJDBC = new MyJDBC();
    @Test
    public void login() {
        int notExpected = -1;
        MyJDBC.dbConnection();
        int id = MyJDBC.login("rosi", "pass321");
        Assert.assertNotEquals(notExpected, id);
    }

    @Test
    public void createAccount() {
    }

    @Test
    public void passwordValidation() {
        String noSpecialSymbol = "Password123";
        Assert.assertEquals(true, user.passwordValidation(noSpecialSymbol));
        String addSpecialSymbol = "Password123!";
        Assert.assertEquals(true, user.passwordValidation(addSpecialSymbol));
        String noCapLetter = "password123";
        Assert.assertEquals(false, user.passwordValidation(noCapLetter));
        String noNumber = "Password!";
        Assert.assertEquals(false, user.passwordValidation(noNumber));
        String tooShort = "pass1";
        Assert.assertEquals(false, user.passwordValidation(tooShort));
        String tooLong = "Password1212345678901";
        Assert.assertEquals(false, user.passwordValidation(tooLong));
        String whiteSpaces1 = "Pass  word123!";
        Assert.assertEquals(false, user.passwordValidation(whiteSpaces1));
        String whiteSpaces2 = "Pass  word123!";
        Assert.assertNotEquals(true, user.passwordValidation(whiteSpaces2));

    }

    @Test
    public void usernameValidation() {
        String noSpecialSymbol = "admin";
        Assert.assertEquals(false, user.usernameValidation(noSpecialSymbol));
        String addSpecialSymbol = "username!";
        Assert.assertEquals(false, user.usernameValidation(addSpecialSymbol));
        String noCapLetter = "username123";
        Assert.assertEquals(true, user.usernameValidation(noCapLetter));
        String noNumber = "username";
        Assert.assertEquals(true, user.usernameValidation(noNumber));
        String tooShort = "user";
        Assert.assertEquals(false, user.usernameValidation(tooShort));
        String tooLong = "username1212345678901";
        Assert.assertEquals(false, user.usernameValidation(tooLong));
        String whiteSpaces1 = "User  name123!";
        Assert.assertEquals(false, user.usernameValidation(whiteSpaces1));
        String whiteSpaces2 = "user name!";
        Assert.assertNotEquals(true, user.usernameValidation(whiteSpaces2));
    }
}