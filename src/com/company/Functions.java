package com.company;

import java.util.Locale;
import java.util.Scanner;

public class Functions {
    MyJDBC MyJDBC = new MyJDBC();

    public int choseFromMenu(){
        int choice = -1;
        try {
            Scanner input = new Scanner(System.in);
            choice = input.nextInt();
        } catch (Exception e) {
            //System.out.println("Error!\nThe value should be a number.\n");
        }
        return choice;
    }

    public int addID(){
        int toAdd = -1;
        try {
            Scanner input = new Scanner(System.in);
            toAdd = input.nextInt();
        } catch (Exception e) {
            //System.out.println("Error!\nThe value should be a number.\n");
        }
        return toAdd;
    }

    public String search(){
        String word = "";
        try {
            Scanner input = new Scanner(System.in);
            word = input.nextLine();
        } catch (Exception e) {
            //System.out.println("Error!\nThe value should be a number.\n");
        }
        return word;
    }

    public String addString(){
        String toAdd = "";
        try {
            Scanner input = new Scanner(System.in);
            toAdd = input.nextLine();
        } catch (Exception e) {
            //System.out.println("Error!\nThe value should be a number.\n");
        }
        return toAdd;
    }

    public double addDouble(){
        double toAdd = 0;
        try {
            Scanner input = new Scanner(System.in).useLocale(Locale.US);
            toAdd = input.nextDouble();
        } catch (Exception e) {
            //System.out.println("Error!\nThe value should be a number.\n");
        }
        return toAdd;
    }

    public void logout(){
        Main main = new Main();
        main.startPage();
    }

    public void exit(){
        MyJDBC.disconnectDB();
        System.exit(0);
    }
}
