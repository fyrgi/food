package com.company;

import java.sql.*;


// this class contains all the SQL statements and methods that the system uses.

public class MyJDBC {
    Connection connection;
    Statement statement;
    ResultSet resultSet;


    public void dbConnection() {

        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/food_app_hkr", "root", "");

            statement = connection.createStatement();

        } catch (Exception e) {
            System.out.println("The database connection failed!");
        }
    }

    // LOGIN, ACCOUNT, PASSWORD CHANGE

    public boolean searchUser(String user){
        boolean flag = false;
        try {
            resultSet = statement.executeQuery("SELECT * FROM user WHERE username = '"+user+"'");
            if(!resultSet.next()){
                flag = false;
            } else {
                flag = true;
            }
        } catch (Exception e) {
            System.out.println("Cannot check the existence of a user!");
        }
        return flag;
    }

    public int login(String user, String pass){
        int user_id = -1;
        try {
            resultSet = statement.executeQuery("SELECT * FROM user WHERE username = '"+user+"' AND password = '"+pass+"'");
            if(!resultSet.next()){
                System.out.println("No match found between username and password.");
            } else {
                do {
                    System.out.println("Hello " + resultSet.getString("first_name") + ", you have successfully logged in\n");
                    user_id = resultSet.getInt(1);
                } while (resultSet.next());
            }
        } catch (Exception e) {
            System.out.println("Cannot retrieve user!");
            System.out.println(e);
        }
        return user_id;
    }

    public void createAccount(String user, String pass, String fName, String lName, String email, String num, String city){
        try {
            statement.executeUpdate("INSERT INTO user VALUES (NULL, '"+user+"', '"+pass+"', '"+fName+"', '"+lName+"', '"+email+"', '"+num+"', '"+city+"')");
            System.out.println("Account with username "+ user + " is created successfully.\nShop smart!\n");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void changePassUser(int id, String newPass){
        try {
            int affected = statement.executeUpdate("UPDATE user SET password = '"+newPass+"'  WHERE id = '"+id+"'");
            if(affected == 1){
                System.out.println("The password was successfully changed.");
            }
        } catch (Exception e) {
            System.out.println("Error 214: Can not change password.\nContact administrator on app@foody.se");
            System.out.println(e);
        }
    }

    public boolean checkPassword(int id, String pass){
        try {
            boolean flag = false;
            resultSet = statement.executeQuery("SELECT * FROM user WHERE id = '"+id+"' " +
                    "AND password = '"+pass+"' ");
            if(resultSet.next()){
                flag = true;
            } else {
                flag = false;
            }
            return flag;
        } catch (Exception e) {
            System.out.println("Error 204: Cna not check the password.\nContact administrator on app@foody.se");
            System.out.println(e);
            return false;
        }
    }

    // ADMIN AND USER FUNCTIONS

    public void displayAllRecords(int choice){
        if(choice == 1){
            searchOffers("**");
        } else if(choice == 2){
            searchItem("**");
        } else if(choice == 3){
            searchCategory("**");
        }else if(choice == 4){
            searchStore("**");
        }
    }

    public void allOnSaleItemsInACategory(String search) {

        try {
            resultSet = statement.executeQuery("SELECT cat.name, i.name, i.vendor, " +
                    "os.current_price, os.original_price, os.available_quantity, qt.name " +
                    "FROM on_sale AS os " +
                    "LEFT JOIN item AS i " +
                    "ON i.id = os.item_id " +
                    "LEFT JOIN item_category AS ic " +
                    "ON ic.item_id = i.id " +
                    "LEFT JOIN quantity_type AS qt " +
                    "ON os.quantity_type_id = qt.id " +
                    "LEFT JOIN category AS cat " +
                    "ON cat.id = ic.category_id " +
                    "WHERE cat.name LIKE '%" + search + "%'" +
                    "OR cat.id LIKE '%"+ search +"%'");
            int rowCount = 0;
            System.out.println("+----------------+----------------+----------------+--------+--------+-----------+");
            System.out.printf("| %-15s| %-15s| %-15s| %-7s| %-7s| %-10s|",
                    "Category", "Product name", "Vendor", "Now for", "Price", "Quantity");
            System.out.println();
            while (resultSet.next()) {
                System.out.println("+----------------+----------------+----------------+--------+--------+-----------+");
                System.out.printf("| %-15s| %-15s| %-15s| %-7s| %-7s| %-10s|",
                        resultSet.getString(1), resultSet.getString(2),
                        resultSet.getString(3), resultSet.getString(4), resultSet.getString(5),
                        resultSet.getString(6) + ' ' + resultSet.getString(7));        ;
                System.out.println();
                rowCount++;
            }
            System.out.println("+----------------+----------------+----------------+--------+--------+-----------+");
            System.out.println(rowCount + " products from category.");
            System.out.println();
        } catch (Exception e) {
            System.out.println("Error 202\nThe search function did not work.\nContact administrator on app@foody.se!\n");
            System.out.println(e);
        }
    }

    public void searchOffers(String search){
        System.out.println(search);
        try {
            if(search.equals("**")){
                resultSet = statement.executeQuery("SELECT os.id, i.name, i.vendor, " +
                        "os.current_price, os.original_price, os.available_quantity, qt.name, " +
                        "s.name, s.city, s.address, s.phone " +
                        "FROM on_sale AS os "+
                        "LEFT JOIN store AS s ON s.id = os.store_id " +
                        "LEFT JOIN item AS i ON i.id = os.item_id " +
                        "LEFT JOIN quantity_type AS qt ON os.available_quantity = qt.id " +
                        "ORDER BY s.city, s.name, i.name");
            } else {
                resultSet = statement.executeQuery("SELECT os.id, i.name, i.vendor, " +
                        "os.current_price, os.original_price, os.available_quantity, qt.name, " +
                        "s.name, s.city, s.address, s.phone " +
                        "FROM on_sale AS os " +
                        "LEFT JOIN store AS s ON s.id = os.store_id " +
                        "LEFT JOIN item AS i ON i.id = os.item_id " +
                        "LEFT JOIN quantity_type AS qt ON os.available_quantity = qt.id " +
                        "WHERE i.name LIKE '%" + search + "%'" +
                        "OR i.vendor LIKE '%" + search + "%'" +
                        "OR s.city LIKE '%" + search + "%'" +
                        "OR os.current_price LIKE '%" + search + "%'" +
                        "ORDER BY s.city, s.name, i.name");
            }
            int rowCount = 0;
            System.out.println("+-----+----------------+----------------+--------+--------+-----------+---------------------+" +
                    "-----------------+-------------------------------+----------------+");
            System.out.printf("| %-4s| %-15s| %-15s| %-7s| %-7s| %-10s| %-20s| %-16s| %-30s| %-15s|",
                    "Code", "Product name", "Vendor", "Now for", "Price", "Quantity" , "Store" , "City" , "Address" , "Phone");
            System.out.println();
            while (resultSet.next()) {
                System.out.println("+-----+----------------+----------------+--------+--------+-----------+---------------------+" +
                        "-----------------+-------------------------------+----------------+");
                System.out.printf("| %-4s| %-15s| %-15s| %-7s| %-7s| %-10s| %-20s| %-16s| %-30s| %-15s|",
                        resultSet.getString(1), resultSet.getString(2),
                        resultSet.getString(3), resultSet.getString(4),
                        resultSet.getString(5),
                        resultSet.getString(6) + ' ' + resultSet.getString(7),
                        resultSet.getString(8), resultSet.getString(9),
                        resultSet.getString(10), resultSet.getString(11));        ;
                System.out.println();
                rowCount++;
            }
            System.out.println("+-----+----------------+----------------+--------+--------+-----------+---------------------+" +
                    "-----------------+-------------------------------+----------------+");
            System.out.println(rowCount + " products on sell.");
            System.out.println();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void searchItem(String search){
        System.out.println(search);
        try {
            if(search.equals("**")){
                resultSet = statement.executeQuery("SELECT * FROM item ORDER BY name asc");
            } else {
                resultSet = statement.executeQuery("SELECT * FROM item WHERE name LIKE '%"+search+"%'" +
                        "OR vendor LIKE '%" + search + "%'" +
                        "OR country_of_production LIKE '%" + search + "%'" +
                        "ORDER BY name asc");
            }
            int rowCount = 0;
            System.out.println("+-----+---------------------+----------------+----------------+");
            System.out.printf("| %-4s| %-20s| %-15s| %-15s|", "Code", "Product name", "Vendor", "Country");
            System.out.println();
            while (resultSet.next()) {
                System.out.println("+-----+---------------------+----------------+----------------+");
                System.out.format("| %-4s| %-20s| %-15s| %-15s|",
                        resultSet.getString(1), resultSet.getString(2),
                        resultSet.getString(4), resultSet.getString(5));
                System.out.println();
                rowCount++;
            }
            System.out.println("+-----+---------------------+----------------+----------------+");
            System.out.println(rowCount + " results match your search.");
            System.out.println();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void searchCategory(String search){
        try {
            if(search.equals("**")){
                resultSet = statement.executeQuery("SELECT * FROM category ORDER BY name asc");
            } else {
                resultSet = statement.executeQuery("SELECT * FROM category WHERE name LIKE '%" + search + "%' ORDER BY name asc");
            }
            int rowCount = 0;
            System.out.println("+------+----------------+");
            System.out.printf("| %-5s| %-15s|", "Code", "Category name");
            System.out.println();
            while (resultSet.next()) {
                System.out.println("+------+----------------+");
                System.out.format("| %-5s| %-15s|",
                        resultSet.getString(1), resultSet.getString(2));
                System.out.println();
                rowCount++;
            }
            System.out.println("+------+----------------+");
            System.out.println(rowCount + " results match your search.");
            System.out.println();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void searchStore(String search){
        try {
            if(search.equals("**")){
                resultSet = statement.executeQuery("SELECT * FROM store ORDER BY name asc");
            } else {
                resultSet = statement.executeQuery("SELECT * FROM store WHERE name LIKE '%" + search + "%' " +
                        "OR city LIKE '%" + search + "%'" +
                        "OR address LIKE '%" + search + "%'" +
                        "OR phone LIKE '%" + search + "%'" +
                        "ORDER BY name asc");
            }
            int rowCount = 0;
            System.out.println("+-----+---------------------+-----------------+-------------------------------+----------------+");
            System.out.printf("| %-4s| %-20s| %-16s| %-30s| %-15s|", "Code", "Store name", "city", "Address", "Phone");
            System.out.println();
            while (resultSet.next()) {
                System.out.println("+-----+---------------------+-----------------+-------------------------------+----------------+");
                System.out.format("| %-4s| %-20s| %-16s| %-30s| %-15s|",
                        resultSet.getString(1), resultSet.getString(2),
                        resultSet.getString(3), resultSet.getString(4),
                        resultSet.getString(5));
                System.out.println();
                rowCount++;
            }
            System.out.println("+-----+---------------------+-----------------+-------------------------------+----------------+");
            System.out.println(rowCount + " results match your search.");
            System.out.println();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void showAllProductsOnSale(){
        try {
            resultSet = statement.executeQuery("SELECT i.name, i.vendor, " +
                    "os.current_price, os.original_price, os.available_quantity, qt.name, " +
                    "s.name, s.city, s.address, s.phone " +
                    "FROM on_sale AS os " +
                    "LEFT JOIN store AS s ON s.id = os.store_id " +
                    "LEFT JOIN item AS i ON i.id = os.item_id " +
                    "LEFT JOIN quantity_type AS qt ON os.quantity_type_id = qt.id " +
                    "ORDER BY s.city, s.name, i.name");
            int rowCount = 0;
            System.out.println("+----------------+----------------+--------+--------+-----------+---------------------+" +
                    "-----------------+-------------------------------+----------------+");
            System.out.printf("| %-15s| %-15s| %-7s| %-7s| %-10s| %-20s| %-16s| %-30s| %-15s|",
                    "Product name", "Vendor", "Now for", "Price", "Quantity" , "Store" , "City" , "Address" , "Phone");
            System.out.println();
            while (resultSet.next()) {
                System.out.println("+----------------+----------------+--------+--------+-----------+---------------------+" +
                        "-----------------+-------------------------------+----------------+");
                System.out.printf("| %-15s| %-15s| %-7s| %-7s| %-10s| %-20s| %-16s| %-30s| %-15s|",
                        resultSet.getString(1), resultSet.getString(2),
                        resultSet.getString(3), resultSet.getString(4),
                        resultSet.getString(5) + ' ' + resultSet.getString(6),
                        resultSet.getString(7), resultSet.getString(8),
                        resultSet.getString(9), resultSet.getString(10));        ;
                System.out.println();
                rowCount++;
            }
            System.out.println("+----------------+----------------+--------+--------+-----------+---------------------+" +
                    "-----------------+-------------------------------+----------------+");
            System.out.println(rowCount + " products on sale.");
            System.out.println();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    // ADMIN FUNCTIONS

    public void searchQuantity(String search){
        try {
            if(search.equals("**")){
                resultSet = statement.executeQuery("SELECT * FROM quantity_type ORDER BY id asc");
            } else {
                resultSet = statement.executeQuery("SELECT * FROM quantity_type WHERE name LIKE '%" + search + "%' ORDER BY name asc");
            }
            int rowCount = 0;
            System.out.println("+------+----------------+");
            System.out.printf("| %-5s| %-15s|", "Code", "Quantity");
            System.out.println();
            while (resultSet.next()) {
                System.out.println("+------+----------------+");
                System.out.format("| %-5s| %-15s|",
                        resultSet.getString(1), resultSet.getString(2));
                System.out.println();
                rowCount++;
            }
            System.out.println("+------+----------------+");
            System.out.println(rowCount + " results match your search.");
            System.out.println();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void addItemsOnSale(int itemID, int storeID, double op, double cp, double available, int qtype, int status){
        Functions func = new Functions();
        String query = "INSERT INTO on_sale VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        String empty = "";
        int id = -1;
        boolean flag = false;

        try {
            PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setNull(1,Types.NULL);
            ps.setInt(2,itemID);
            ps.setInt(3,storeID);
            ps.setDouble(4,op);
            ps.setDouble(5,cp);
            ps.setTimestamp(6, java.sql.Timestamp.from(java.time.Instant.now()));
            ps.setDouble(7,available);
            ps.setInt(8,qtype);
            ps.setInt(9,1);
            ps.execute();
            ResultSet rs = ps.getGeneratedKeys();

            while (rs.next()){
                id = rs.getInt(1);
                System.out.println("Sale with ID "+id+" is added successfully.");
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void addItem(String name, String vendor, String country){
        Functions func = new Functions();
        String query = "INSERT INTO item VALUES (?, ?, ?, ?, ?, ?)";
        String empty = "";
        int id = -1;
        boolean flag = false;
        try {
            PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                ps.setNull(1,Types.NULL);
                ps.setString(2,name);
                ps.setString(3,empty);
                ps.setString(4,vendor);
                ps.setString(5,country);
                ps.setString(6,empty);
                ps.execute();
            ResultSet rs = ps.getGeneratedKeys();
            while (rs.next()){
                id = rs.getInt(1);
                System.out.println("Product "+ name + " with ID "+id+" is added successfully." +
                        "Add the item to at least one category now");
            }
            if(id != -1 || flag != false){
                do {
                    displayAllRecords(2);
                    System.out.println("To add new category please enter 0");
                    int cat = func.addID();
                    if(cat == 0){
                        AdminFunctions admin = new AdminFunctions();
                        int newCatID = admin.addCategory();
                        flag = addItemToCategory(id,newCatID);
                    } else {
                        System.out.println("Adding the product "+ name +" to a corresponding category");
                        flag = addItemToCategory(id,cat);
                    }

                } while (!flag);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public boolean addItemToCategory(int itemID, int categoryID){
        try {
            statement.executeUpdate("INSERT INTO item_category VALUES (NULL, '"+itemID+"', '"+categoryID+"')");
            resultSet = statement.executeQuery("SELECT i.name, cat.name FROM item_category AS ic " +
                    "LEFT JOIN item AS i " +
                    "ON ic.item_id = i.id " +
                    "LEFT JOIN category AS cat " +
                    "ON ic.category_id = cat.id " +
                    "WHERE ic.item_id = "+itemID+"");

            while (resultSet.next()){
                System.out.println("Product "+resultSet.getString(1)+" is added successfully to "+resultSet.getString(2)+".\n");
            }
            return true;
        } catch (Exception e) {
            System.out.println("The item was not added to a category," +
                    " please write a code from the below.");
            return false;
        }
    }

    public int addCategory(String name){
        String query = "INSERT INTO category VALUES (NULL, '"+name+"')";
        int id = -1;
        try {
            PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.execute();
            ResultSet rs = ps.getGeneratedKeys();
            while (rs.next()){
                id = rs.getInt(1);
                System.out.println("Category "+ name + " with ID "+id+" was added successfully.");
            }
            return id;
        } catch (Exception e) {
            System.out.println(e);
            return id;
        }
    }

    public void addStore(String name, String city, String address, String phone){
        try {
            int rows = statement.executeUpdate("INSERT INTO store VALUES (NULL, '"+name+"', '"+city+"', '"+address+"', '"+phone+"')");
            if(rows == 1){
                System.out.println("Store with name "+ name + " in "+ city+ " is created successfully.\n");
            }
        } catch (Exception e) {
            System.out.println("Error 143!\nCan not add into store.");
            System.out.println(e);
        }
    }

    public void deleteOffer(int offerID){
        String query = "DELETE FROM on_sale WHERE id = '"+offerID+"'";
        try{
            int rows = statement.executeUpdate(query);
            if(rows > 0){
                System.out.println("The offer was deleted.\n");
            } else {
                System.out.println("The offer was not deleted.\n");
            }
        } catch (Exception e) {
            System.out.println("Error 114!\nCould not delete from on_sale");
            System.out.println(e);
        }
    }

    public void deleteItemFull(int itemID){
        String query = "DELETE FROM item WHERE id = '"+itemID+"'";
        try{
            deleteItemFromCategory(itemID);
            deleteItemFromSaleList(itemID);
            int rows = statement.executeUpdate(query);
            if(rows > 0){
                System.out.println("The product was deleted.\n");
            } else {
                System.out.println("The product was not deleted.\n");
            }
        } catch (Exception e) {
            System.out.println("Error 124!\nCould not delete from item");
            System.out.println(e);
        }
    }

    public void deleteItemFromCategory(int itemID){
        String query = "DELETE FROM item_category WHERE item_id = '"+itemID+"'";
        try{
            statement.executeUpdate(query);
        } catch (Exception e) {
            System.out.println("Error 1241!\nCould not delete from item_category.");
            System.out.println(e);
        }
    }

    public void deleteItemFromSaleList(int itemID){
        String query = "DELETE FROM on_sale WHERE item_id = '"+itemID+"'";
        try{
            statement.executeUpdate(query);
        } catch (Exception e) {
            System.out.println("Error 1242!\nCould not delete from on_sale.");
        }
    }

    public void deleteCategory(int categoryID){
        String query = "DELETE FROM category WHERE id = '"+categoryID+"'";
        try{
            int rows = statement.executeUpdate(query);
            if(rows > 0){
                System.out.println("The category was deleted.\n");
            } else {
                System.out.println("The category was not deleted.\n");
            }
        } catch (Exception e) {
            System.out.println("Error 134!\nCould not delete from category.");
        }
    }

    public void deleteStore(int StoreID){
        String query = "DELETE FROM store WHERE id = '"+StoreID+"'";
        try{
            deleteStoreFromSaleList(StoreID);
            int rows = statement.executeUpdate(query);
            if(rows > 0){
                System.out.println("The store was deleted.\n");
            } else {
                System.out.println("The store was not deleted.\n");
            }
        } catch (Exception e) {
            System.out.println("Error 144!\nCould not delete from store");
            System.out.println(e);
        }
    }

    public void deleteStoreFromSaleList(int StoreID){
        String query = "DELETE FROM on_sale WHERE store_id = '"+StoreID+"'";
        try{
            statement.executeUpdate(query);
        } catch (Exception e) {
            System.out.println("Error 1441!\nCould not delete from on_sale.");
        }
    }

    public void disconnectDB(){
        try {
            if (connection != null)
                connection.close();
            if (statement != null)
                statement.close();
            if (resultSet != null)
                connection.close();
        } catch (SQLException exception) {
            System.out.println("Failed to disconnect!");
        }
    }
}
