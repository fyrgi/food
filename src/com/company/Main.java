package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        startPage();

    }
    public static void startPage(){
        MyJDBC MyJDBC = new MyJDBC();
        MyJDBC.dbConnection();
        Validations valid = new Validations();
        Functions func = new Functions();
        StartMenu startMenu = new StartMenu();

        User user = new User();
        int option = 0;
        int session = -1;
        boolean flag = false;
        boolean system = true;
        do {
            System.out.println(startMenu.toString());

            do {
                startMenu.showMenu();
                System.out.println("\nPlease enter your choice: >>> ");
                option = func.choseFromMenu();
                flag = valid.isValidMenuOption(option, 3);
            } while (!flag);

            if (option == 1) {
                session = user.login();
            } else if (option == 2) {
                user.createAccount();
            } else if (option == 3){
                System.out.println("You are exiting the system.\nSee you soon!");
                func.exit();
            }
            // user admin pass 1
            if(session == 1){
                AdminMenu aMenu = new AdminMenu();
                aMenu.showMenu();
                system = false;
            } else if (session != -1){
                UserMenu uMenu = new UserMenu();
                uMenu.showMenu(session);
                system = false;
            }
        } while (system);
    }
}
