package com.company;

public class AdminMenu {
    MyJDBC MyJDBC = new MyJDBC();
    Validations valid = new Validations();
    Functions func = new Functions();
    AdminFunctions admin = new AdminFunctions();
    String singular;


    public String showSubMenu(String singular){
        String subMenu = "<<<=== Chose your actions for "+ singular +" ===>>>\n"
                + "\t1. View all records\n"
                + "\t2. Search for "+ singular +"\n"
                + "\t3. Add new "+ singular +"\n"
                + "\t4. Delete a "+ singular +"\n"
                + "\t5. Back to admin menu"
                + "\nPlease enter your choice: >>> ";
        return subMenu;
    }

    public String showAdminMenu(){
        String menu = "<<<=== Select you options ===>>>\n"
                + "\t1. On sale products\n"
                + "\t2. Products\n"
                + "\t3. Categories\n"
                + "\t4. Stores\n"
                + "\t5. Change password\n"
                + "\t6. Logout\n"
                + "\t7. Exit\n"
                + "Please enter your choice: >>> ";
        return menu;
    }

    public String singular(int choice){
        if(choice == 1){
            singular = "products on sale";
        } else if(choice == 2){
            singular = "product";
        } else if(choice == 3){
            singular = "category";
        } else if (choice == 4){
            singular = "store";
        }
        return singular;
    }

    public void showMenu(){
        int userChoice;
        int subChoice = -1;
        boolean wrong_choice = true;
        MyJDBC.dbConnection();

        do {
            System.out.println(this.showAdminMenu());

            userChoice = func.choseFromMenu();
            wrong_choice = valid.isValidMenuOption(userChoice, 7);

            if (userChoice >= 1 && userChoice <= 4) {
                // this one is rotating while the admin choice
                // is a menu option between 1 and 4 including them.
                do {
                    // get the singular word of the subcategory that is being worked on
                    // to build up the next submenu.

                    do {
                        singular = this.singular(userChoice);
                        System.out.println(this.showSubMenu(singular));
                        subChoice = func.choseFromMenu();
                        wrong_choice = valid.isValidMenuOption(subChoice, 5);
                    } while (wrong_choice == false);
                    // these are the choices that will work directly with the database
                    // everything here should be some sort of queries.
                    // Time might not be enough so I will try to implement user choice No1
                    if (userChoice == 1) {
                        switch (subChoice) {
                            case 1:
                                System.out.println("\n<<<=== All products on sale ===>>>");
                                MyJDBC.showAllProductsOnSale();
                                break;
                            case 2:
                                System.out.println("\n<<<=== Search within all products on sale ===>>>");
                                System.out.println("You can search within name, vendor, city");
                                System.out.println("Use ** to display all records: >>>");
                                String search = func.search();
                                MyJDBC.searchOffers(search);
                                break;
                            case 3:
                                System.out.println("\n<<<=== Add a new offer  ===>>>");
                                admin.addSale();
                                break;
                            case 4:
                                System.out.println("\n<<<=== Delete an offer by id  ===>>>");
                                admin.deleteOffer();
                                break;
                            case 5:
                                break;
                            default:
                                System.out.println("Not a valid sub menu choice");
                        }

                    } else if (userChoice == 2) {
                        switch (subChoice) {
                            case 1:
                                System.out.println("\n<<<=== All "+singular+"(s) ===>>>");
                                MyJDBC.displayAllRecords(userChoice);
                                break;
                            case 2:
                                System.out.println("\n<<<=== Search within "+singular+"(s) name(s) ===>>>");
                                System.out.println("You can search within name, vendor, country of production");
                                System.out.println("Use ** to display all records: >>>");
                                String user = func.search();
                                MyJDBC.searchItem(user);
                                break;
                            case 3:
                                System.out.println("\n<<<=== Add a product to the catalog ===>>>" );
                                admin.addItem();
                                break;
                            case 4:
                                System.out.println("\n<<<=== Delete a product from the list  ===>>>\n" +
                                        "The product will be deleted from categories and on sale items");
                                admin.deleteItem();
                                break;
                            case 5:
                                break;
                            default:
                                System.out.println("Not a valid sub menu choice");
                        }

                    } else if (userChoice == 3) {
                        switch (subChoice) {
                            case 1:
                                System.out.println("\n<<<=== All "+singular+"(s)===>>>");
                                MyJDBC.displayAllRecords(userChoice);
                                break;
                            case 2:
                                System.out.println("\n<<<=== Search within "+singular+"(s) name(s) ===>>>");
                                System.out.println("Use ** to display all records: >>>");
                                String user = func.search();
                                MyJDBC.searchCategory(user);
                                break;
                            case 3:
                                System.out.println("\n<<<=== Add "+singular+" ===>>>");
                                int newCat = admin.addCategory();
                                break;
                            case 4:
                                System.out.println("\n<<<=== Delete "+singular+" ===>>>");
                                admin.deleteCategory();
                                break;
                            case 5:
                                break;
                            default:
                                System.out.println("Not a valid sub menu choice");
                        }
                    } else if (userChoice == 4) {
                        switch (subChoice) {
                            case 1:
                                System.out.println("\n<<<=== All "+singular+"(s) ===>>>");
                                MyJDBC.displayAllRecords(userChoice);
                                break;
                            case 2:
                                System.out.println("\n<<<=== Search for "+singular+"(s) ===>>>");
                                System.out.println("You can search within name, phone, address or city");
                                System.out.println("Use ** to display all records: >>>");
                                String user = func.search();
                                MyJDBC.searchStore(user);
                                break;
                            case 3:
                                System.out.println("\n<<<=== Add "+singular+" ===>>>");
                                admin.addStore();
                                break;
                            case 4:
                                System.out.println("\n<<<=== Delete "+singular+" ===>>>");
                                admin.deleteStore();
                                break;
                            case 5:
                                break;
                            default:
                                System.out.println("Not a valid sub menu choice");
                        }
                    }
                } while (subChoice >=1 && subChoice <=4);


            }
            if(userChoice == 5) {
                MyJDBC.dbConnection();
                System.out.println("\nYou are changing your password.");
                System.out.println("Enter your current password: >>>");
                String oldPass = func.search();
                boolean oldCheck = MyJDBC.checkPassword(1, oldPass);
                if(oldCheck == false){
                    System.out.println("Wrong current password");
                } else {
                    System.out.println("Enter your new password: >>>");
                    String newPass = func.addString();
                    User user = new User();
                    boolean passCheck = user.passwordValidation(newPass);
                    if(passCheck == false){
                        System.out.println("Invalid password choice.\n");
                    } else if (passCheck == true){
                        MyJDBC.changePassUser(1, newPass);
                    }
                }
            }
        } while (userChoice < 6 || wrong_choice == false);

        if(userChoice == 6){
            System.out.println("You have logged out successfully!");
            func.logout();
        } else if (userChoice == 7) {
            System.out.println("You are exiting the system.\nSee you soon!");
            func.exit();
        }
    }
}
