package com.company;

import java.util.Scanner;

public class UserMenu {
    MyJDBC MyJDBC = new MyJDBC();
    Validations valid = new Validations();
    Functions func = new Functions();

    public void showMenu(int user_id){
        int userChoice;
        int current_user = user_id;
        boolean wrong_choice = true;
        do {
            System.out.println("Select you options");
            System.out.println("\t1. See all products on sale");
            System.out.println("\t2. Show all products from category");
            System.out.println("\t3. Search for a product on sale");
            System.out.println("\t4. Change password");
            System.out.println("\t5. Logout");
            System.out.println("\t6. Exit");
            System.out.println("\nPlease enter your choice: >>> ");

            userChoice = func.choseFromMenu();
            wrong_choice= valid.isValidMenuOption(userChoice, 6);

            if (userChoice == 1){
                MyJDBC.dbConnection();
                System.out.println("<<<=== All current offers ===>>> ");
                MyJDBC.showAllProductsOnSale();

            } else if (userChoice == 2){
                MyJDBC.dbConnection();
                System.out.println("<<<=== All categories ===>>> ");
                MyJDBC.displayAllRecords(3);
                System.out.println("<<<=== Search for items on sale from a specific category ===>>> ");
                System.out.println("You can use both name or code from the list below");
                String search = func.search();
                MyJDBC.allOnSaleItemsInACategory(search);

            } else if (userChoice == 3){

                MyJDBC.dbConnection();
                System.out.println("<<<=== Search for products on sale ===>>> ");
                System.out.println("You can search within name, vendor, country of production");
                System.out.println("Use ** to display all records: >>>");
                String user = func.search();
                MyJDBC.searchOffers(user);

            } else if (userChoice == 4){
                MyJDBC.dbConnection();
                System.out.println("\n<<<=== Change your password ===>>>");
                System.out.println("Enter your current password: >>>");
                String oldPass = func.search();
                boolean oldCheck = MyJDBC.checkPassword(current_user, oldPass);
                if(oldCheck == false){
                    System.out.println("Wrong current password!");
                } else {
                    System.out.println("Enter your new password: >>>");
                    String newPass = func.addString();
                    User user = new User();
                    boolean passCheck = user.passwordValidation(newPass);
                    if(passCheck == false){
                        System.out.println("Invalid password choice!\n");
                    } else if (passCheck == true){
                        MyJDBC.changePassUser(current_user, newPass);
                    }
                }
            }
        } while (userChoice < 5 || wrong_choice == false);
        if (userChoice == 5){

            System.out.println("You have logged out successfully!\n");
            func.logout();

        } else if (userChoice == 6) {

            System.out.println("You are exiting the system.\nSee you soon!");
            func.exit();

        }
    }
}
