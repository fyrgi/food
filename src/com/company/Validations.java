package com.company;

public class Validations {

    public boolean isValidMenuOption(int input, int numberOptions){
        if(input < 1 || input > numberOptions){
            System.out.println("Your choice is out of the range.\nTry again.\n");
            return false;
        } else {
            return true;
        }
    }
}
