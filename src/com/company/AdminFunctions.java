package com.company;

public class AdminFunctions {
    MyJDBC MyJDBC = new MyJDBC();
    Functions func = new Functions();

    public void addSale(){
        MyJDBC.dbConnection();
        boolean flag = false;
        int itemID = -1;
        int storeID = -1;
        int qType = -1;
        double quantity = 0;
        double originalPrice = 0;
        double currentPrice = 0;
        System.out.println("This function requires IDs.\n" +
                "Whenever you don't have the id write \"0\" and take the result from column code: >>>");
        // product id
        do {
            System.out.println("ID of the product you want to add to a sale: >>>");
            itemID = func.addID();
            if(itemID == 0){
                System.out.println("Use ** to show all or search for a product");
                String search = func.search();
                MyJDBC.searchItem(search);
            } else {
                flag = true;
            }
        } while(!flag);

        flag = false;
        // store id
        do {
            System.out.println("ID of the store the sale is being added to: >>>");
            storeID = func.addID();
            if(storeID == 0){
                System.out.println("Use ** to show all or search for a store");
                String search = func.search();
                MyJDBC.searchStore(search);
            } else {
                flag = true;
            }
        } while(!flag);

        flag = false;

        System.out.println("The original selling price of the product.\n" +
                "Use '.' if you are adding a decimal (*.00): >>>");
        originalPrice = func.addDouble();

        System.out.println("The new reduced price of the product.\n" +
                "Use '.' if you are adding a decimal (*.00): >>>");
        currentPrice = func.addDouble();

        System.out.println("Quantity of the selling item.\n" +
                "Use '.' if you are adding a decimal (*.00): >>>");
        quantity = func.addDouble();
        // quantity type
        do {
            System.out.println("ID of the quantity type the item uses: >>>");
            qType = func.addID();
            if(qType == 0){
                System.out.println("Use ** to show all or search for quantity types");
                String search = func.search();
                MyJDBC.searchQuantity(search);
            } else {
                flag = true;
            }
        } while(!flag);

        MyJDBC.addItemsOnSale(itemID,storeID,originalPrice,currentPrice, quantity,qType,1);
    }

    public void addItem(){
        MyJDBC.dbConnection();
        String name = "";
        System.out.println("Enter product name: >>>");
        do {
            name = func.addString();
        } while(name == "");
        System.out.println("Enter vendor: >>>");
        String vendor = func.addString();
        System.out.println("Enter country or production: >>>");
        String country = func.addString();
        MyJDBC.addItem(name,vendor,country);
    }

    public int addCategory(){
        MyJDBC.dbConnection();
        System.out.println("Enter the new category's name: >>>");
        String category = func.addString();
        int id = -1;
        id = MyJDBC.addCategory(category);
        return id;
    }

    public void addStore(){
        MyJDBC.dbConnection();
        String name = "";
        System.out.println("Enter the stores' name: >>>");
        do {
            name = func.addString();
        } while(name == "");
        System.out.println("Enter address: >>>");
        String address = func.addString();
        System.out.println("Enter city: >>>");
        String city = func.addString();
        System.out.println("Enter phone: >>>");
        String phone = func.addString();
        MyJDBC.addStore(name, city, address, phone);
    }

    public void deleteOffer(){
        MyJDBC.dbConnection();
        boolean flag = false;
        int offerID = -1;
        System.out.println("This function requires the offer's ID.\n" +
                "Whenever you don't have the id write \"0\" and take the result from column code.\n");
        do {
            System.out.println("ID of the offer you want to delete: >>>");
            offerID = func.addID();
            if(offerID == 0){
                System.out.println("Use ** to show all or search for an offer");
                String search = func.search();
                MyJDBC.searchOffers(search);
            } else {
                flag = true;
            }
        } while(!flag);
        MyJDBC.deleteOffer(offerID);
    }

    public void deleteItem(){
        MyJDBC.dbConnection();
        boolean flag = false;
        int itemID = -1;
        System.out.println("This function requires the item's ID.\n" +
                "Whenever you don't have the id write \"0\" and take the result from column code.\n");
        do {
            System.out.println("ID of the product you want to delete: >>>");
            System.out.println("Use ** to show all or search for a product");
            itemID = func.addID();
            if(itemID == 0){
                String search = func.search();
                MyJDBC.searchItem(search);
            } else {
                flag = true;
            }
        } while(!flag);
        MyJDBC.deleteItemFull(itemID);
    }

    public void deleteCategory(){
        MyJDBC.dbConnection();
        boolean flag = false;
        int categoryID = -1;
        System.out.println("This function requires the ID of the category.\n" +
                "Whenever you don't have the id write \"0\" and take the result from column code.\n");
        do {
            System.out.println("ID of the category you want to delete: >>>");

            categoryID = func.addID();
            if(categoryID == 0){
                System.out.println("Use ** to show all or search for a category");
                String search = func.search();
                MyJDBC.searchCategory(search);
            } else {
                flag = true;
            }
        } while(!flag);
        MyJDBC.deleteCategory(categoryID);
    }

    public void deleteStore(){
        MyJDBC.dbConnection();
        boolean flag = false;
        int storeID = -1;
        System.out.println("This function requires the ID of the store.\n" +
                "Whenever you don't have the id write \"0\" and take the result from column code.\n");
        do {
            System.out.println("ID of the store you want to delete: >>>");
            storeID = func.addID();
            if(storeID == 0){
                System.out.println("Use ** to show all or search for a store");
                String search = func.search();
                MyJDBC.searchStore(search);
            } else {
                flag = true;
            }
        } while(!flag);
        MyJDBC.deleteStore(storeID);
    }

}
