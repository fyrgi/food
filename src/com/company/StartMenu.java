package com.company;

public class StartMenu {

    public void showMenu(){
        String menu = "Login or create an account to see our current offers\n"
                + "\t1. Login\n"
                + "\t2. Create an account\n"
                + "\t3. Exit";
        System.out.println();
        System.out.println(menu);
    }

    @Override
    public String toString() {
        return "<<<=== Welcome to Foody - the environment helping app! ===>\n(beta version)";
    }
}
