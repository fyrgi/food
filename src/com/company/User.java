package com.company;

import java.util.Scanner;
import java.util.regex.*;

public class User {
    MyJDBC MyJDBC = new MyJDBC();

    public int login(){
        int user_id = -1;

        MyJDBC.dbConnection();

        System.out.println("Enter username: ");
        Scanner user = new Scanner(System.in);
        String username = user.next();
        System.out.println("Enter password: ");
        Scanner pass = new Scanner(System.in);
        String password = pass.next();
        user_id = MyJDBC.login(username, password);
        return user_id;
    }

    public void createAccount(){
        MyJDBC.dbConnection();
        boolean flag = false;
        String username;
        String password;
        System.out.println("The username must be between 6 and 20 symbols long\n" +
                "It can not contain white spaces and special characters.\n");
        do {
            System.out.println("Enter username: ");
            Scanner user = new Scanner(System.in);
            username = user.next();
            flag = usernameValidation(username);
            if(flag) {
                if (MyJDBC.searchUser(username)) {
                    System.out.println("That username is already taken. Please choose another.");
                    flag = false;
                } else {
                    flag = true;
                }
            }
        } while(!flag);
        System.out.println("The password must be between 6 and 20 symbols long.\n"
                +"It can not contain white spaces. Must have at least 1 capital letter, 1 small letter. May have special symbol");
        do {
            System.out.println("Enter password: ");
            Scanner pass = new Scanner(System.in);
            password = pass.next();
            flag = passwordValidation(password);
        } while(!flag);
        // no validation made for the rest of the data.
        // if there was a table with cities then the person might have been forced to choose between them
        // there are regular expression for email validation. if there is time I might implement more checks
        System.out.println("Your first name: ");
        Scanner first = new Scanner(System.in);
        String firstname = first.next();
        System.out.println("Your last name: ");
        Scanner last = new Scanner(System.in);
        String lastname = last.next();
        System.out.println("Your email address: ");
        Scanner mail = new Scanner(System.in);
        String email = mail.next();
        System.out.println("Phone number ");
        Scanner number = new Scanner(System.in);
        String phone = number.next();
        System.out.println("The city you live in ");
        Scanner stad = new Scanner(System.in);
        String city = stad.next();
        MyJDBC.createAccount(username, password, firstname,lastname,email,phone,city);
        Main start = new Main();
        start.startPage();

    }
    public boolean passwordValidation(String password) {
        // source: https://www.geeksforgeeks.org/how-to-validate-a-password-using-regular-expressions-in-java/

        // Regex to check validity of password.
        // Must contain a number, a capital letter, a special symbol and to be at least 6 and max 18 symbols long
        // white spaces are not allowed anywhere in the field
        String regex = "^(?=.*\\w)"
                + "(?=.*[0-9])"
                + "(?=.*[a-z])(?=.*[A-Z])"
                + "(?=\\S+$).{6,20}$";

        // Compile the ReGex
        Pattern p = Pattern.compile(regex);

        // If the password is empty
        // return false
        if (password == null) {
            return false;
        }

        // Pattern class contains matcher() method
        // to find matching between given password
        // and regular expression.
        Matcher m = p.matcher(password);
        // Return true or false
        return m.matches();
    }

    public boolean usernameValidation(String username){
        // source: https://www.geeksforgeeks.org/how-to-validate-a-password-using-regular-expressions-in-java/
        // no special symbols and no spaces.
        String regex = "^(?=\\w+$)"
                + "(?=\\S+$).{6,20}$";


        Pattern p = Pattern.compile(regex);

        if (username == null) {
            return false;
        }

        Matcher m = p.matcher(username);

        return m.matches();
    }

}
